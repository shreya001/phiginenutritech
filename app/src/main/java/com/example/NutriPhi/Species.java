package com.example.NutriPhi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Species extends AppCompatActivity {
    CardView first_card;
    CardView second_card;
    CardView third_card;
    CardView forth_card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_species);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        first_card = findViewById(R.id.poultry);
        second_card = findViewById(R.id.Cattle);
        third_card = findViewById(R.id.swine);
        forth_card = findViewById(R.id.aqua);

        first_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Species.this, Poultry_info.class);
                startActivity(intent);
            }
        });

        second_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Species.this, Cattle_info.class);
                startActivity(intent);
            }
        });

        third_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Species.this, Swine_info.class);
                startActivity(intent);
            }
        });

        forth_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Species.this, Aqua_info.class);
                startActivity(intent);
            }
        });
    }
}
